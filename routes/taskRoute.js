const express = require('express');
const router = express.Router();

const TaskController = require('../controllers/taskController')

// Get all the task
router.get('/', (req, res) => {
	TaskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

// Creating a task
router.post('/', (req, res) => {
	TaskController.createTask(req.body).then(result => res.send(result));
})

// Delete a task
// URL "http://localhost:3001/task/:id"
// params parameters
router.delete("/:id", (req, res) => {
	TaskController.deleteTask(req.params.id).then(result => res.send(result));
})

// Update a task
router.put("/:id", (req, res) => {
	TaskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})

// Get a specific task
router.get("/:id", (req, res) => {
	TaskController.getOneTask(req.params.id).then(result => res.send(result));
})

// Update task status
router.put("/:id/complete",(req, res) => {
	TaskController.updateTaskStatus(req.params.id).then(result => res.send(result));
})

module.exports = router;

