const Task = require('../models/taskModel');

//Controller Function for getting all the tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then( result => {
		return result;
	})
};


// Creating a task
module.exports.createTask = (requestBody) => {

	//Create object
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error) {
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}


// Deleting a task
// "id" URL paramter passed from the taskRoute.js

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err) {
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	})
}


// Update a task
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, err) => {
		if(err) {
			console.log(error);
			return false;
		}

		result.name = newContent.name;

		return result.save().then((updatedTask, saveErr) => {
			if(saveErr) {
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})


		
	})
}


//Get Specific Task
module.exports.getOneTask = (taskId) => {
	return Task.findById(taskId).then((result,err) => {
		if(err) {
			console.log(err);
			return false;
		}
		else {
			return result;
		}
	})
}


// Update Task Status
/*
	Business Logic
	1. Get the id using the findById
	2. Chagne the status of the document to complete
	3. Save
*/
module.exports.updateTaskStatus = (taskId) => {
	return Task.findById(taskId).then((result, err) => {
		if(err) {
			console.log(err);
			return error;
		}  
		
		result.status = "complete.";
		return result.save().then((updatedTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})
		
	})
}